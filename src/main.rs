extern crate notify_rust;
extern crate chrono;

use std::time::SystemTime;
use notify_rust::Notification;
use chrono::{DateTime, Local};

fn alert(t: DateTime<Local>) {
    Notification::new()
        .appname("Clock notification")
        .summary("Clock")
        .body(&format!("{}", t.to_rfc2822()))
        .finalize()
        .show()
        .unwrap();
}

fn main() {
    let t = DateTime::from(SystemTime::now());
    alert(t)
}
